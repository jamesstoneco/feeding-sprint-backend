from dotenv import load_dotenv
import os
from flask import Flask, request, jsonify, make_response, abort
from flask_cors import CORS

# google sheets api v4 related
# from __future__ import print_function
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import pandas as pd


load_dotenv()

CORS_PATTERN = r"/*"

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.

SAMPLE_SPREADSHEET_ID = os.environ.get("GOOGLE_SPREADSHEET_ID")
SAMPLE_RANGE_NAME = 'FeedingCleanData!A1:I' # can't use I because there are blanks returned as none

def create_app():
    app = Flask(__name__, static_url_path='', static_folder='static')
    cors = CORS(app, resources={CORS_PATTERN: {"origins": "*"}})

    # allow authenticated cross-origin requests
    @app.after_request
    def apply_allow_credentials(response):
        response.headers['Access-Control-Allow-Credentials'] = 'true'
        return response

    def error_response(statusCode = 400, message = 'Error occured'):
        responseData = jsonify({'message': message})
        return make_response(responseData, statusCode)

    def get_google_sheet():
        """Shows basic usage of the Sheets API.
        Prints values from a sample spreadsheet.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        # Call the Sheets API
        sheet = service.spreadsheets() # pylint: disable=no-member
        result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                    range=SAMPLE_RANGE_NAME).execute()
        return result

    def gsheet2df(gsheet):
        # source: https://towardsdatascience.com/how-to-access-google-sheet-data-using-the-python-api-and-convert-to-pandas-dataframe-5ec020564f0e
        """ Converts Google sheet data to a Pandas DataFrame.
        Note: This script assumes that your data contains a header file on the first row!
        Also note that the Google API returns 'none' from empty cells - in order for the code
        below to work, you'll need to make sure your sheet doesn't contain empty cells,
        or update the code to account for such instances.
        """
        header = gsheet.get('values', [])[0]   # Assumes first line is header!
        values = gsheet.get('values', [])[1:]  # Everything else is data.
        if not values:
            print('Error gsheet2df: No data found.')
        else:
            all_data = []
            for col_id, col_name in enumerate(header):
                column_data = []
                for row in values:
                    column_data.append(row[col_id])
                ds = pd.Series(data=column_data, name=col_name)
                all_data.append(ds)
            df = pd.concat(all_data, axis=1)
            df[["date", "time"]] = df[["date", "time"]].apply(pd.to_datetime)
            df[["breast", "bottle", "tube", "bmf", "pegorion", "vitamins"]] = df[["breast", "bottle", "tube", "bmf", "pegorion", "vitamins"]].apply(pd.to_numeric)
            # df[["comments"]] = df[["comments"]].apply(pd.astype('str'))
            return df

    @app.route('/feedings', methods=['GET'])
    def get_data():
        feeding_sprint_sheet = get_google_sheet()
        df = gsheet2df(feeding_sprint_sheet)
        # return df.to_json(orient = 'table')
        return df.describe().to_json(orient = 'table')
    @app.route('/')
    def root():
        return 'feeding sprint api v1.0.0'

    return app

if __name__ == "__main__":
    create_app().run(debug=True, host='0.0.0.0', port=8000)
