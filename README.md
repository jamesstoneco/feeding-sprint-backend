# Feeding Sprint Backend

Feeding Sprint is a POC app based on a google sheet that I co-created with my partner Maria Stone to track our childs feeding schedule post NICU discharge. The goal is to develop a better UI for data entry and utilities for example for timing, as well as visualize or otherwise explore the data.

## Installation

Follow the directions on the [Google API v4 quickstart](https://developers.google.com/sheets/api/quickstart/python?authuser=1). When you click the button *Enable the Google Sheets API* be sure to download the credentials.json. Then add that file to the root of this repo.

Run the app locally. Authenticate in the web browser.

Create .env file and set the GOOGLE_SPREADSHEET_ID in it.

## Example Google Sheet

You can use the following google sheet as an example

https://docs.google.com/spreadsheets/d/1tbWU5c4o_hQxEQrj2m354Fb-cODvC7eV-lTW88Wq73I/edit?usp=sharing

Sheet ID 1tbWU5c4o_hQxEQrj2m354Fb-cODvC7eV-lTW88Wq73I

## Running the development environment

From the project root

```bash
pipenv install
pipenv shell
python app.py
```

Requires Python 3.5.x to run correctly.

```bash
pipenv install --python=3.5.2
```

This might be required if you are using pyenv to manage several python versions.